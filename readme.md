#Модуль оплаты платежной системы IntellectMoney для CMS CS Cart

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/CS-Cart#cscart-files.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/CS-Cart#557841c02f4bd80f1e41e8892465786000f9e5
