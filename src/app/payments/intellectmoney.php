<?php

use Tygh\Http;
use Tygh\Registry;
use Tygh\Storage;

include_once 'IntellectMoneyHelper/MerchantReceiptHelper.php';

if (!defined('AREA')) {
    die('Access denied');
}

if (defined('PAYMENT_NOTIFICATION')) {

    if ($mode == 'result') {

        function ob_exit($status = null) {
            if ($status) {
                ob_end_flush();
                isset($_REQUEST['debug']) ? exit($status) : exit();
            } else {
                ob_end_clean();
                header("HTTP/1.0 200 OK");
                echo "OK";
                exit();
            }
        }

        function debug_file() {
            header('Content-type: text/plain; charset=utf-8');
            echo file_get_contents(__FILE__);
        }

        function check_im() {
            $iplist = gethostbynamel('www.intellectmoney.ru');
            foreach ($iplist as $ip)
                if (preg_match("/^$ip/", (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : ''))
                    return true;
            return false;
        }

        function from_request($name) {
            return isset($_REQUEST[$name]) ? htmlspecialchars(iconv('windows-1251', 'utf-8', $_REQUEST[$name])) : null;
        }

        /* main check */
        if (isset($_REQUEST['debug_file'])) {
            debug_file();
            exit;
        }
        ob_start();

        $eshopId = from_request('eshopId');
        $orderId = from_request('orderId');
        $serviceName = from_request('serviceName');
        $eshopAccount = from_request('eshopAccount');
        $recipientAmount = from_request('recipientAmount');
        $recipientCurrency = from_request('recipientCurrency');
        $paymentStatus = from_request('paymentStatus');
        $userName = from_request('userName');
        $userEmail = from_request('userEmail');
        $paymentData = from_request('paymentData');
        $secretKey = from_request('secretKey');
        $hash = from_request('hash');

        if (!isset($_REQUEST['orderId'])) {
            ob_exit("ERROR: EMPTY REQUEST!\n");
        }

        $order_info = fn_get_order_info($orderId);

        $processor_data = fn_get_payment_method_data($order_info['payment_id']);

        $to_pay = im_get_price($order_info['total'], $processor_data['processor_params']['im_test_live_mode']);

        // ORDER ID CHECK
        if (!$order_info || !is_array($to_pay)) {
            ob_exit("ERROR: ORDER NOT EXISTS OR TO_PAY EOF!\n");
        }

        $get_amount = $to_pay[0];
        $get_currency = $to_pay[1];
        $get_eshopid = $processor_data['processor_params']['im_ehopid'];
        $get_secret_key = $processor_data['processor_params']['im_secretkey'];
        $get_customer_contact = $processor_data['processor_params']['im_customer_contact'];

        // AMOUNT and CURRENCY CODE CHECK
        if (strtolower($recipientCurrency) != strtolower($get_currency)) {
            $err = "ERROR: CURRENCY MISMATCH!\n";
            $err .= "recipientAmount: $recipientAmount; amount: $get_amount;\nrecipientCurrency: $recipientCurrency; currency: $get_currency;\n\n";
            ob_exit($err);
        }

        if ($secretKey) {
            if ($secretKey != $get_secret_key) {
                $err = "ERROR: SECRET_KEY MISMATCH!\n";
                $err .= check_im() ? ("secretKey: $secretKey; IM_SECRET_KEY: $get_secret_key;\n\n") : "\n";
                ob_exit($err);
            }

            if ($eshopId != $get_eshopid) {
                $err = "ERROR: INCORRECT ESHOP_ID!\n";
                $err .= "eshopId: $eshopId; IM_ESHOP_ID: $get_eshopid;\n\n";
                ob_exit($err);
            }
        } else {
            $control_hash_str = implode('::', array(
                $get_eshopid,
                $orderId,
                $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency,
                $paymentStatus, $userName, $userEmail, $paymentData,
                $get_secret_key,
            ));
            $control_hash = md5($control_hash_str);
            $control_hash_1251 = md5(iconv('utf-8', 'windows-1251', $control_hash_str));
            if (($hash != $control_hash && $hash != $control_hash_1251) || !$hash) {
                $err = "ERROR: HASH MISMATCH\n";
                $err .= check_im() ? "Control hash string: $control_hash_str;\n" : "";
                $err .= "Control hash utf-8: $control_hash;\nControl hash win-1251: $control_hash_1251;\nhash: $hash;\n\n";
                ob_exit($err);
            }
        }

        $orders_statuses = array(
            '3' => array(
                "CMSStatus" => $processor_data['processor_params']['Created'],
                "UnAvailableStatuses" => array($processor_data['processor_params']['Paid'], $processor_data['processor_params']['Canceled'], $processor_data['processor_params']['PartiallyPaid'], $processor_data['processor_params']['Holded'])
            ),
            '4' => array(
                "CMSStatus" => $processor_data['processor_params']['Canceled'],
                "UnAvailableStatuses" => array($processor_data['processor_params']['Refunded'])
            ),
            '5' => array(
                "CMSStatus" => $processor_data['processor_params']['Paid'],
                "UnAvailableStatuses" => array($processor_data['processor_params']['Canceled'])
            ),
            '6' => array(
                "CMSStatus" => $processor_data['processor_params']['Holded'],
                "UnAvailableStatuses" => array($processor_data['processor_params']['Canceled'], $processor_data['processor_params']['Paid'])
            ),
            '7' => array(
                "CMSStatus" => $processor_data['processor_params']['PartiallyPaid'],
                "UnAvailableStatuses" => array($processor_data['processor_params']['Paid'], $processor_data['processor_params']['Canceled'], $processor_data['processor_params']['Holded'])
            ),
            '8' => array(
                "CMSStatus" => $processor_data['processor_params']['Refunded'],
                "UnAvailableStatuses" => array("")
            ),
        );
        if (!in_array($order_info['status'], $orders_statuses[from_request('paymentStatus')]['UnAvailableStatuses'])) {
            $pp_response = array();
            $pp_response['reason_text'] = date('m.d.Y H:i:s');
            $pp_response['order_status'] = $orders_statuses[from_request('paymentStatus')]['CMSStatus'];
            $pp_response['invoice_id'] = from_request('paymentId');
            fn_update_order_payment_info($orderId, $pp_response);
            fn_change_order_status($orderId, $pp_response['order_status']);
            fn_finish_payment($orderId, $pp_response);
        }

        ob_exit();
    } elseif ($mode == 'unsupported_currency') {

        if (fn_check_payment_script('intellectmoney.php', $_REQUEST['orderId'])) {
            $pp_response = array();
            $pp_response['order_status'] = 'F';
            $pp_response['reason_text'] = fn_get_lang_var('im_text_unsupported_currency');

            fn_finish_payment($_REQUEST['orderId'], $pp_response);
            fn_order_placement_routines($_REQUEST['orderId']);
        }
    } elseif ($mode == 'success'){
		fn_order_placement_routines('route', $_REQUEST['order_id']);
	}
} else {

    $current_location = Registry::get('config.current_location');

    $to_pay = im_get_price($order_info['total'], $processor_data['processor_params']['im_test_live_mode']);
    if (!isset($to_pay) || !is_array($to_pay)) {
        fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('im_text_unsupported_currency'));
        fn_redirect(Registry::get('config.current_location') . "/$index_script?dispatch=payment_notification.unsupported_currency&payment=intellectmoney&order_id=$orderId");
        exit;
    }
    $eshop_id = $processor_data['processor_params']['im_ehopid'];
    $item_name = $processor_data['processor_params']['im_order_prefix'] . $order_id;
    $item_amount = number_format($to_pay[0], 2, '.', '');
    $currency_code = $to_pay[1];
    $result = "$current_location/$index_script?dispatch=payment_notification.result&payment=intellectmoney";
    $success = $processor_data['processor_params']['im_success_url'];
    $fail = "$current_location/$index_script?dispatch=payment_notification.fail&payment=intellectmoney";
    $get_inn = $processor_data['processor_params']['im_inn'];
    $delivery_tax = $processor_data['processor_params']['im_delivery_tax'];
    $tax = $processor_data['processor_params']['im_tax'];
    $hold_mode = $processor_data['processor_params']['im_hold_mode'];
    $preference = $processor_data['processor_params']['im_preference'];
    $expire_date = $processor_data['processor_params']['im_expire_date'];
    $group = !empty($processor_data['processor_params']['im_group']) ? $processor_data['processor_params']['im_group'] : NULL;
    $expire_date = date('Y-m-d H:i:s', strtotime('+' . getExpiredHours($expire_date) . ' hour'));
    $hold_time = $hold_mode ? getHoldHours($hold_time) : 0;
    $control_hash = $eshop_id . "::" . $order_id . "::" . $item_name . "::" . $item_amount . "::" . $currency_code . '::' . $processor_data['processor_params']['im_secretkey'];
    $hash = md5($control_hash);

    $merchant_reciept = generateMerchantReceipt($order_info, $item_amount, $get_inn, $delivery_tax, $tax, $group);
    $form_data["c_description{$suffix}"] = $v['product'];

    //  Формирование формы
    $url = 'https://merchant.intellectmoney.ru/ru/';
    $post = array();
    if (!empty($order_info['invoice_id'])) {
        $post['invoiceId'] = $order_info['invoice_id'];
    } else {
        $post['eshopId'] = $eshop_id;
        $post['orderId'] = $order_id;
        $post['serviceName'] = $item_name;
        $post['recipientAmount'] = $item_amount;
        $post['userEmail'] = $order_info['email'];
        $post['username'] = $order_info['s_firstname'] . " " . $order_info['s_lastname'];
        $post['recipientCurrency'] = $currency_code;
        $post['hash'] = $hash;
        $post['resultUrl'] = $result;
        $post['successUrl'] = $success;
        $post['failUrl'] = $fail;
        $post['merchantReceipt'] = $merchant_reciept;
        $post['holdTime'] = $hold_time;
        $post['expireDate'] = $expire_date;
        $post['holdMode'] = $hold_mode;
        $post['preference'] = $preference;
    }

    // Empty cart
    $_SESSION['cart'] = array(
        'user_data' => !empty($_SESSION['cart']['user_data']) ? $_SESSION['cart']['user_data'] : array(),
        'profile_id' => !empty($_SESSION['cart']['profile_id']) ? $_SESSION['cart']['profile_id'] : 0,
        'user_id' => !empty($_SESSION['cart']['user_id']) ? $_SESSION['cart']['user_id'] : 0,
    );
    $_SESSION['shipping_rates'] = array();
    unset($_SESSION['shipping_hash']);

    fn_create_payment_form($url, $post, 'IntellectMoney');
    exit;
}

function im_get_price($price, $im_test_live_mode) {
    $currencies = Registry::get('currencies');
    if (empty($currencies['RUB']) && empty($currencies['TST'])) {
        return false;
    }
    $rub = false;
    $tst = false;
    if (isset($currencies['RUB']['is_primary']) && $currencies['RUB']['is_primary'] == 'Y' && !$im_test_live_mode)
        $rub = true;
    if (isset($currencies['TST']['is_primary']) && $currencies['TST']['is_primary'] == 'Y' && $im_test_live_mode)
        $tst = true;
    if (!$rub && !$tst) {
        return false;
    }
    if (empty($currencies[!$im_test_live_mode ? 'RUB' : 'TST']['coefficient']))
        return false;
    return array(fn_format_price($price / $currencies[!$im_test_live_mode ? 'RUB' : 'TST']['coefficient']), $rub ? 'RUB' : 'TST');
}

function generateMerchantReceipt($order_info, $item_amount, $get_inn, $delivery_tax, $tax, $group) {
    $products = $order_info['products'];
    $userEmail = $order_info['email'];
    $merchant_helper = new PaySystem\MerchantReceiptHelper($item_amount, $get_inn, $userEmail, $group, 0, 0);
    foreach ($products as $key => $product) {
        $merchant_helper->addItem($product['price'], $product['amount'], $product['product'], $tax);
    }
    if (!empty($order_info['display_shipping_cost']) && $order_info['display_shipping_cost'] > 0) {
        $merchant_helper->addItem($order_info['display_shipping_cost'], 1, "Доставка", $delivery_tax);
    }
    return $merchant_helper->generateMerchantReceipt(false);
}

function getExpiredHours($days) {
    return (intval($days) && $days > 0 && $days < 4319) ? $days : 4319;
}

function getHoldHours($hours) {
    return (intval($hours) && $hours > 0 && $hours < 119) ? $hours : 72;
}

?>