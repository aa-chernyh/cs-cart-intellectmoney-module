{* $Id: webmoney.tpl 0002 2009-05-08 18:44:04Z murzik $ *}

<div>
    <p>{__("im_text_desc")}</p>
</div>
<hr />

<div class="form-field">
    <label for="im_ehopid">{__("im_ehopid")}:</label>
    <input type="text" name="payment_data[processor_params][im_ehopid]" id="im_ehopid" value="{$processor_params.im_ehopid}" class="input-text" size="60" />
</div>

<div class="form-field">
    <label for="im_secretkey">{__("im_secretkey")}:</label>
    <input type="text" name="payment_data[processor_params][im_secretkey]" id="im_secretkey" value="{$processor_params.im_secretkey}" class="input-text" size="60" />
</div>

<div class="form-field">
    <label for="im_test_live_mode">{__("im_test_live_mode")}:</label>
    <select name="payment_data[processor_params][im_test_live_mode]" id="im_test_live_mode">
        <option value="0" {if $processor_params.im_test_live_mode == "0"}selected="selected"{/if}>{__("im_live")}</option>
        <option value="1" {if $processor_params.im_test_live_mode == "1"}selected="selected"{/if}>{__("im_test")}</option>
    </select>
</div>

<div class="form-field">
    <label for="im_order_prefix">{__("im_order_prefix")}:</label>
    <input type="text" name="payment_data[processor_params][im_order_prefix]" id="im_order_prefix" value="{$processor_params.im_order_prefix}" class="input-text" size="60" />
</div>

<div class="form-field">
    <label for="im_inn">{__("im_inn")}:</label>
    <input type="text" name="payment_data[processor_params][im_inn]" id="im_inn" value="{$processor_params.im_inn}" class="input-text" size="60" />
</div>

<div class="form-field" style="padding: 10px 0;">
    <label style="display: inline;" for="im_hold_mode">{__("im_hold_mode")}:</label>
    <input style="display: inline; margin: -3px 0 0 5px;" type="checkbox" name="payment_data[processor_params][im_hold_mode]" id="im_hold_mode" class="input-text" value="1" {if $processor_params.im_hold_mode == "1"}checked{/if}/>
</div>

<div class="form-field">
    <label for="im_expire_date">{__("im_expire_date")}:</label>
    <input type="text" name="payment_data[processor_params][im_expire_date]" id="im_expire_date" value="{$processor_params.im_expire_date}" class="input-text" size="60" />
</div>

<div class="form-field">
    <label for="im_hold_time">{__("im_hold_time")}:</label>
    <input type="text" name="payment_data[processor_params][im_hold_time]" id="im_hold_time" value="{$processor_params.im_hold_time}" class="input-text" size="60" />
</div>

<div class="form-field">
    <label for="im_success_url">{__("im_success_url")}:</label>
    <input type="text" name="payment_data[processor_params][im_success_url]" id="im_success_url" value="{$processor_params.im_success_url}" class="input-text" size="60" />
</div>

<div class="form-field">
    <label for="im_delivery_tax">{__("im_delivery_tax")}:</label>
    <select name="payment_data[processor_params][im_delivery_tax]" id="im_delivery_tax" class="input-text">
        <option value="1" {if $processor_params.im_delivery_tax == "1"}selected="selected"{/if}>ставка НДС 20%</option>
        <option value="2" {if $processor_params.im_delivery_tax == "2"}selected="selected"{/if}>ставка НДС 10%</option>
        <option value="3" {if $processor_params.im_delivery_tax == "3"}selected="selected"{/if}>ставка НДС расч. 20/120</option>
        <option value="4" {if $processor_params.im_delivery_tax == "4"}selected="selected"{/if}>ставка НДС расч. 10/110</option>
        <option value="5" {if $processor_params.im_delivery_tax == "5"}selected="selected"{/if}>ставка НДС 0%</option>
        <option value="6" {if $processor_params.im_delivery_tax == "6"}selected="selected"{/if}>НДС не облагается</option>
    </select>
</div>

<div class="form-field">
    <label for="im_tax">{__("im_tax")}:</label>
    <select name="payment_data[processor_params][im_tax]" id="im_tax" class="input-text">
        <option value="1" {if $processor_params.im_tax == "1"}selected="selected"{/if}>ставка НДС 20%</option>
        <option value="2" {if $processor_params.im_tax == "2"}selected="selected"{/if}>ставка НДС 10%</option>
        <option value="3" {if $processor_params.im_tax == "3"}selected="selected"{/if}>ставка НДС расч. 20/120</option>
        <option value="4" {if $processor_params.im_tax == "4"}selected="selected"{/if}>ставка НДС расч. 10/110</option>
        <option value="5" {if $processor_params.im_tax == "5"}selected="selected"{/if}>ставка НДС 0%</option>
        <option value="6" {if $processor_params.im_tax == "6"}selected="selected"{/if}>НДС не облагается</option>
    </select>
</div>

<div class="form-field">
    <label for="im_preference">{__("im_preference")}:</label>
    <input type="text" name="payment_data[processor_params][im_preference]" id="im_hold_time" value="{$processor_params.im_preference}" class="input-text" size="60" />
</div>  

<div class="form-field">
    <label for="im_group">{__("im_group")}:</label>
    <input type="text" name="payment_data[processor_params][im_group]" id="im_hold_time" value="{$processor_params.im_group}" class="input-text" size="60" />
</div>    

{assign var="statuses" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses}
{assign var='im_statuses' value=['Created' => 'Создан', 'Canceled' => 'Отменен', 'Paid' => 'Оплачен','Holded' => 'Захолдирован', 'PartiallyPaid' => 'Частично оплачен', 'Refunded' => 'Возвращен']}

<div class="form-field" style="padding: 5px 0 0 0;">
    <label style="display: inline; font-weight: bold;">Статусы IntellectMoney</label>
    <label style="display: inline; margin-left: 123px; font-weight: bold;">Статусы магазина</label>
</div>  

{foreach from=$im_statuses item="im_status_name" key="im_status_number"}
    <div class="form-field">
        <label class="im_status_{$im_status_number}" style="display: inline-block; width: 270px;"> {$im_status_name}:</label>
        <div class="statuses" style="display: inline-block;">
            <select name="payment_data[processor_params][{$im_status_number}]" id="{$im_status_number}">
                {foreach from=$statuses item="cms_status_name" key="cms_status_key"}
                    <option value="{$cms_status_key}" {if $processor_params.$im_status_number == $cms_status_key}selected="selected"{/if}>{$cms_status_name}</option>
                {/foreach}
            </select>
        </div>
    </div>
{/foreach}

